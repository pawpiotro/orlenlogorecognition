#include "pch.h"
#include "Segment.h"



Segment::Segment()
{
}


Segment::Segment(cv::Vec3b _color)
{
	color = _color;
}

double Segment::m(int p, int q) {
	double moment = 0;
	for (std::list<cv::Point2i>::iterator it = points.begin(); it != points.end(); ++it) {
		moment += pow(it->y + 1, p) * pow(it->x + 1, q);
	}
	return moment;
}

double Segment::getArea() {
	m00 = points.size();
	return m00;
}

void Segment::extractFeatures() {
	minY = 10000;
	maxY = 0;
	minX = 10000;
	maxX = 0;
	for (std::list<cv::Point2i>::iterator it = points.begin(); it != points.end(); ++it) {
		if (it->y > maxY)
			maxY = it->y;
		if (it->y < minY)
			minY = it->y;
		if (it->x > maxX)
			maxX = it->x;
		if (it->x < minX)
			minX = it->x;
	}
	height = maxY - minY;

	//ordinary moments
	double m01 = m(0, 1);
	double m02 = m(0, 2);
	double m03 = m(0, 3);
	double m10 = m(1, 0);
	double m11 = m(1, 1);
	double m12 = m(1, 2);
	double m20 = m(2, 0);
	double m21 = m(2, 1);
	double m30 = m(3, 0);

	_i = m10 / m00;
	_j = m01 / m00;

	//central moments
	double M02 = m02 - pow(m01, 2)/m00;
	double M03 = m03 - 3*m02*_j + 2*m01*pow(_j,2);
	double M11 = m11 - m10 * m01/m00;
	double M12 = m12 - 2*m11*_j - m02*_i + 2*m10*pow(_j,2);
	double M20 = m20 - pow(m10, 2)/m00;
	double M21 = m21 - 2*m11*_i - m20*_j + 2*m01*pow(_i,2);
	double M30 = m30 - 3*m20*_i + 2*m10*pow(_i,2);

	MI1 = (M20 + M02) / pow(m00,2);
	MI2 = (pow((M20 - M02),2) + 4*pow(M11,2)) / pow(m00,4);
	MI3 = (pow((M30 - 3*M12),2) + pow((3*M21 - M03),2)) / pow(m00,5);
	MI7 = (M20*M02 - pow(M11,2)) / pow(m00,4);

	MI4 = (pow((M30 + M12), 2) + pow((M21 + M03), 2)) / pow(m00, 5);
	//MI5 = ((M30 - 3*M12)*(M30 + M12)*(pow((M30 + M12), 2) - 3*pow((M21 + M03),2)) +
	//	(3*M21 - M03)*(M21 + M03)*(3*pow((M30 + M12), 2) - pow((M21 + M03), 2))) / pow(m00, 10);
	//MI6 = ((M20 - M02)*(pow((M30+M12),2) - pow((M21+M03),2)) + 4*M11*(M30+M12)*(M21+M03)) / pow(m00,7);
	//MI8 = (M30*M12 + M21 * M03 - pow(M12, 2) - pow(M21, 2)) / pow(m00, 5);
	//MI9 = (M20*(M21*M03 - pow(M12,2)) + M02*(M03*M12 - pow(M21,2)) - M11*(M30*M03 - M21*M12)) / pow(m00,7);
	//MI10 = (pow((M30*M03 - M12*M21),2) - 4*(M30*M12 - pow(M21,2))*(M03*M21 - M12)) / pow(m00, 10);
}

Segment::~Segment()
{
}
