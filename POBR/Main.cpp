#include "pch.h"
#include <iostream>
#include "ImageRecognition.h"

int main(int, char *[]) {
	std::cout << "Start ..." << std::endl;
	cv::Mat image = cv::imread("./InputData/orlen_5.jpg");
	if (image.empty()) {
		std::cout << "Invalid image." << std::endl;
		return 1;
	}
	
	ImageRecognition imageRecognition = ImageRecognition();
	if (imageRecognition.recognize(image)) {
		std::cout << "Orlen logo successfully found." << std::endl;
	}
	else {
		std::cout << "Orlen logo not recognized in the image." << std::endl;
	}

	//cv::imshow("orlen", image);
	cv::waitKey(-1);
	return 0;
}


void featureTesting() {
	ImageRecognition imageRecognition = ImageRecognition();
	std::cout << "O" << std::endl;
	cv::Mat image = cv::imread("./InputData/O.jpg");
	imageRecognition.recognize(image);
	std::cout << std::endl;

	std::cout << "R" << std::endl;
	image = cv::imread("./InputData/R.jpg");
	imageRecognition.recognize(image);
	std::cout << std::endl;

	std::cout << "L" << std::endl;
	image = cv::imread("./InputData/L.jpg");
	imageRecognition.recognize(image);
	std::cout << std::endl;

	std::cout << "E" << std::endl;
	image = cv::imread("./InputData/E.jpg");
	imageRecognition.recognize(image);
	std::cout << std::endl;

	std::cout << "N" << std::endl;
	image = cv::imread("./InputData/N.jpg");
	imageRecognition.recognize(image);
	std::cout << std::endl;
}
