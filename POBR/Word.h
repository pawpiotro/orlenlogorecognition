#pragma once
#include <list>
#include "opencv2/core/core.hpp"
#include "Segment.h"

class Word
{
public:
	Word();
	~Word();

	Segment O, R, L, E, N;
	int minX, maxX, minY, maxY;
	void boundingBox();
};

