#pragma once
#include <list>
#include "opencv2/core/core.hpp"

class Segment
{
public:
	std::list<cv::Point2i> points;
	cv::Vec3b color;
	
	double m00; //area

	//moment invariants
	double MI1, MI2, MI3, MI7,
		MI4, MI5, MI6, MI8, MI9, MI10;

	int height;
	int minX, maxX, minY, maxY;

	//image center
	double _i;
	double _j;

	Segment();
	Segment(cv::Vec3b color);
	~Segment();
	void extractFeatures();
	double getArea();
	double m(int p, int q);
};

