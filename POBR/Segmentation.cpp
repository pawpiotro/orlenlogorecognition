#include "pch.h"
#include "Segmentation.h"


const cv::Vec3b Segmentation::MIN_WHITE_LESS_RESTRICTIVE = cv::Vec3b(0, 125, 0);
const cv::Vec3b Segmentation::MIN_WHITE = cv::Vec3b(0, 180, 0);
const cv::Vec3b Segmentation::MAX_WHITE = cv::Vec3b(180, 255, 255);
const cv::Vec3b Segmentation::MIN_RED = cv::Vec3b(170, 80, 80);
const cv::Vec3b Segmentation::MAX_RED = cv::Vec3b(10, 255, 255);


Segmentation::Segmentation()
{
}


std::list<Segment> Segmentation::performSegmentation(cv::Mat &I) {
	std::list<Segment> segments;

	cv::Mat hlsImage;
	cv::cvtColor(I, hlsImage, CV_BGR2HLS);
	cv::Mat newImage = extractRedAndConvertGray(hlsImage);

	ImageProcessing processor = ImageProcessing();

	cv::Mat imageClone = newImage.clone();
	cv::Mat afterFloodFill = doFloodFill(imageClone, 0, 0, 0, 100);
	cv::Mat afterMedianFilter = processor.medianFilter(afterFloodFill, 5);
	cv::Mat extracted = extractFromBackground(hlsImage, afterMedianFilter, 100);
	cv::Mat forTextRecognition = extractPotentialText(extracted);

	cv::imshow("afterRedExtraction", newImage);
	cv::imshow("afterFloodFill", afterFloodFill);
	cv::imshow("afterMedianFilter", afterMedianFilter);
	cv::imshow("extracted", extracted);
	cv::imshow("forTextRecognition", forTextRecognition);

	segments = getSegments(forTextRecognition);
	cv::imshow("forTextRecognition - after segmentation", forTextRecognition);

	return segments;
}

std::list<Segment> Segmentation::performSegmentationWithNoExtraction(cv::Mat &I) {
	std::list<Segment> segments;

	cv::Mat hlsImage;
	cv::cvtColor(I, hlsImage, CV_BGR2HLS);
	cv::Mat forTextRecognition = extractPotentialText(hlsImage);
	cv::imshow("extracted", forTextRecognition);

	segments = getSegments(forTextRecognition);
	cv::imshow("forTextRecognition - after segmentation", forTextRecognition);

	return segments;
}

std::list<Segment> Segmentation::performSegmentationWithNoExtractionLessRestrictive(cv::Mat &I) {
	std::list<Segment> segments;

	cv::Mat hlsImage;
	cv::cvtColor(I, hlsImage, CV_BGR2HLS);
	cv::Mat forTextRecognition = extractPotentialTextLessRestrictive(hlsImage);
	cv::imshow("extracted", forTextRecognition);

	segments = getSegments(forTextRecognition);
	cv::imshow("forTextRecognition - after segmentation", forTextRecognition);

	return segments;
}

std::list<Segment> Segmentation::getSegments(cv::Mat& I) {
	std::list<Segment> segments;
	cv::Vec3b inputColor = cv::Vec3b(255, 255, 255);
	srand(time(NULL));

	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat_<cv::Vec3b> _I = I;
	cv::Mat res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
		case 3: {
			for (int i = 0; i < I.rows; ++i) {
				for (int j = 0; j < I.cols; ++j) {
					if ((_I(i, j)[0] == inputColor[0] && _I(i, j)[1] == inputColor[1] && _I(i, j)[2] == inputColor[2])) {
						segments.push_back(doFloodFillSegmentation(I, j, i, inputColor, cv::Vec3b(rand()%255, rand() % 255, rand() % 255)));
					}
				}
			}
			break;
		}
	}

	return segments;
}


cv::Mat Segmentation::extractPotentialText(const cv::Mat& I) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat_<cv::Vec3b> _I = I;
	cv::Mat res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
		case 3: {
			for (int i = 0; i < I.rows; ++i) {
				for (int j = 0; j < I.cols; ++j) {
					if (isWhite(_I(i, j))) {
						res.at<cv::Vec3b>(i, j)[0] = 255;
						res.at<cv::Vec3b>(i, j)[1] = 255;
						res.at<cv::Vec3b>(i, j)[2] = 255;
					}
					else {
						res.at<cv::Vec3b>(i, j)[0] = 0;
						res.at<cv::Vec3b>(i, j)[1] = 0;
						res.at<cv::Vec3b>(i, j)[2] = 0;
					}

				}
			}
			break;
		}
	}
	return res;
}

cv::Mat Segmentation::extractPotentialTextLessRestrictive(const cv::Mat& I) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat_<cv::Vec3b> _I = I;
	cv::Mat res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3: {
		for (int i = 0; i < I.rows; ++i) {
			for (int j = 0; j < I.cols; ++j) {
				if (isWhiteMoreless(_I(i, j))) {
					res.at<cv::Vec3b>(i, j)[0] = 255;
					res.at<cv::Vec3b>(i, j)[1] = 255;
					res.at<cv::Vec3b>(i, j)[2] = 255;
				}
				else {
					res.at<cv::Vec3b>(i, j)[0] = 0;
					res.at<cv::Vec3b>(i, j)[1] = 0;
					res.at<cv::Vec3b>(i, j)[2] = 0;
				}

			}
		}
		break;
	}
	}
	return res;
}

cv::Mat Segmentation::extractRedAndConvertGray(const cv::Mat& I) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat_<cv::Vec3b> _I = I;
	cv::Mat res(I.rows, I.cols, CV_8UC1);
	switch (I.channels()) {
		case 3: {
			for (int i = 0; i < I.rows; ++i) {
				for (int j = 0; j < I.cols; ++j) {
					if (isRed(_I(i, j))) {
						res.at<uchar>(i, j) = 255;
					}
					else {
						res.at<uchar>(i, j) = 0;
					}
				}
			}
			break;
		}
	}
	return res;
}

bool Segmentation::isWhite(cv::Vec3b _I) {
	if ((_I[0] >= MIN_WHITE[0] && _I[0] <= MAX_WHITE[0]) && (_I[1] >= MIN_WHITE[1] && _I[1] <= MAX_WHITE[1]) && (_I[2] >= MIN_WHITE[2] && _I[2] <= MAX_WHITE[2])) {
		return true;
	}
	else {
		return false;
	}
}

bool Segmentation::isWhiteMoreless(cv::Vec3b _I) {
	if ((_I[0] >= MIN_WHITE_LESS_RESTRICTIVE[0] && _I[0] <= MAX_WHITE[0]) && (_I[1] >= MIN_WHITE_LESS_RESTRICTIVE[1] && _I[1] <= MAX_WHITE[1]) && (_I[2] >= MIN_WHITE_LESS_RESTRICTIVE[2] && _I[2] <= MAX_WHITE[2])) {
		return true;
	}
	else {
		return false;
	}
}

bool Segmentation::isRed(cv::Vec3b _I) {
	if ((_I[0] >= MIN_RED[0] || _I[0] <= MAX_RED[0]) && (_I[1] >= MIN_RED[1] && _I[1] <= MAX_RED[1]) && (_I[2] >= MIN_RED[2] && _I[2] <= MAX_RED[2])) {
		return true;
	}
	else {
		return false;
	}
}



Segment Segmentation::doFloodFillSegmentation(cv::Mat& I, int x, int y, cv::Vec3b oldColor, cv::Vec3b newColor) {
	CV_Assert(I.depth() != sizeof(uchar));
	Segment res = Segment(newColor);
	switch (I.channels()) {
		case 3: {
			cv::Mat_<cv::Vec3b> _I = I;
			int height = I.rows;
			int width = I.cols;
			std::list<cv::Point2i> points;
			points.push_back(cv::Point2i(x, y));
			cv::Point2i tmp, up, down, left, right;

			while (!points.empty()) {
				tmp = points.front();
				points.pop_front();

				_I(tmp)[0] = newColor[0];
				_I(tmp)[1] = newColor[1];
				_I(tmp)[2] = newColor[2];
				
				res.points.push_back(tmp);

				up = cv::Point2i(tmp.x, tmp.y - 1);
				down = cv::Point2i(tmp.x, tmp.y + 1);
				left = cv::Point2i(tmp.x - 1, tmp.y);
				right = cv::Point2i(tmp.x + 1, tmp.y);


				if (left.x >= 0 && find(points.begin(), points.end(), left) == points.end()) {
					if (_I(left)[0] == oldColor[0] && _I(left)[1] == oldColor[1] && _I(left)[2] == oldColor[2]) {
						points.push_back(left);
					}
				}

				if (right.x < width && find(points.begin(), points.end(), right) == points.end()) {
					if (_I(right)[0] == oldColor[0] && _I(right)[1] == oldColor[1] && _I(right)[2] == oldColor[2]) {
						points.push_back(right);
					}
				}

				if (up.y >= 0 && find(points.begin(), points.end(), up) == points.end()) {
					if (_I(up)[0] == oldColor[0] && _I(up)[1] == oldColor[1] && _I(up)[2] == oldColor[2]) {
						points.push_back(up);
					}
				}

				if (down.y < height && find(points.begin(), points.end(), down) == points.end()) {
					if (_I(down)[0] == oldColor[0] && _I(down)[1] == oldColor[1] && _I(down)[2] == oldColor[2]) {
						points.push_back(down);
					}
				}
			}
			I = _I;
			break;
		}
	}

	return res;


}


cv::Mat Segmentation::doFloodFill(cv::Mat& I, int x, int y, cv::Vec3b oldColor, cv::Vec3b newColor) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
		case 3: {
			cv::Mat_<cv::Vec3b> _I = I;
			int height = I.rows;
			int width = I.cols;
			std::list<cv::Point2i> points;
			points.push_back(cv::Point2i(x, y));
			cv::Point2i tmp, up, down, left, right;

			while (!points.empty()) {
				tmp = points.front();
				points.pop_front();

				_I(tmp)[0] = newColor[0];
				_I(tmp)[1] = newColor[1];
				_I(tmp)[2] = newColor[2];

				up = cv::Point2i(tmp.x, tmp.y - 1);
				down = cv::Point2i(tmp.x, tmp.y + 1);
				left = cv::Point2i(tmp.x - 1, tmp.y);
				right = cv::Point2i(tmp.x + 1, tmp.y);


				if (left.x >= 0 && find(points.begin(), points.end(), left) == points.end()) {
					if (_I(left)[0] == oldColor[0] && _I(left)[1] == oldColor[1] && _I(left)[2] == oldColor[2]) {
						points.push_back(left);
					}
				}

				if (right.x < width && find(points.begin(), points.end(), right) == points.end()) {
					if (_I(right)[0] == oldColor[0] && _I(right)[1] == oldColor[1] && _I(right)[2] == oldColor[2]) {
						points.push_back(right);
					}
				}

				if (up.y >= 0 && find(points.begin(), points.end(), up) == points.end()) {
					if (_I(up)[0] == oldColor[0] && _I(up)[1] == oldColor[1] && _I(up)[2] == oldColor[2]) {
						points.push_back(up);
					}
				}

				if (down.y < height && find(points.begin(), points.end(), down) == points.end()) {
					if (_I(down)[0] == oldColor[0] && _I(down)[1] == oldColor[1] && _I(down)[2] == oldColor[2]) {
						points.push_back(down);
					}
				}
			}
			res = _I;
			break;
		}
	}
	return res;


}


cv::Mat Segmentation::doFloodFill(cv::Mat& I, int x, int y, uchar oldColor, uchar newColor) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat res(I.rows, I.cols, CV_8UC1);
	switch (I.channels()) {
		case 1: {
			cv::Mat_<uchar> _I = I;
			int height = I.rows;
			int width = I.cols;
			std::list<cv::Point2i> points;
			points.push_back(cv::Point2i(x, y));
			cv::Point2i tmp, up, down, left, right;

			while (!points.empty()) {
				tmp = points.front();
				points.pop_front();

				_I(tmp) = newColor;

				up = cv::Point2i(tmp.x, tmp.y - 1);
				down = cv::Point2i(tmp.x, tmp.y + 1);
				left = cv::Point2i(tmp.x - 1, tmp.y);
				right = cv::Point2i(tmp.x + 1, tmp.y);


				if (left.x >= 0 && find(points.begin(), points.end(), left) == points.end()) {
					if (_I(left) == oldColor) {
						points.push_back(left);
					}
				}

				if (right.x < width && find(points.begin(), points.end(), right) == points.end()) {
					if (_I(right) == oldColor) {
						points.push_back(right);
					}
				}

				if (up.y >= 0 && find(points.begin(), points.end(), up) == points.end()) {
					if (_I(up) == oldColor) {
						points.push_back(up);
					}
				}

				if (down.y < height && find(points.begin(), points.end(), down) == points.end()) {
					if (_I(down) == oldColor) {
						points.push_back(down);
					}
				}
			}
			res = _I;
			break;
		}
	}
	return res;


}

cv::Mat Segmentation::extractFromBackground(cv::Mat& I, cv::Mat& flooded, cv::Vec3b color) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat res(I.rows, I.cols, CV_8UC3);

	switch (I.channels()) {
		case 3: {
			cv::Mat_<cv::Vec3b> _I = I;
			cv::Mat_<cv::Vec3b> _f = flooded;
			cv::Mat_<cv::Vec3b> _R = res;
			for (int x = 0; x < I.rows; ++x) {
				for (int y = 0; y < I.cols; ++y) {
					if (_f(x, y)[0] == color[0] && _f(x, y)[1] == color[1] && _f(x, y)[2] == color[2]) {
						_R(x, y)[0] = 0;
						_R(x, y)[1] = 0;
						_R(x, y)[2] = 0;
					}
					else {
						_R(x, y)[0] = _I(x, y)[0];
						_R(x, y)[1] = _I(x, y)[1];
						_R(x, y)[2] = _I(x, y)[2];
					}
				}
			}
			res = _R;
			break;
		}
	}
	return res;

}



cv::Mat Segmentation::extractFromBackground(cv::Mat& I, cv::Mat& flooded, uchar color) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat res(I.rows, I.cols, CV_8UC3);

	switch (I.channels()) {
		case 3: {
			cv::Mat_<cv::Vec3b> _I = I;
			cv::Mat_<uchar> _f = flooded;
			cv::Mat_<cv::Vec3b> _R = res;
			for (int x = 0; x < I.rows; ++x) {
				for (int y = 0; y < I.cols; ++y) {
					if (_f(x, y) == color) {
						_R(x, y)[0] = 0;
						_R(x, y)[1] = 0;
						_R(x, y)[2] = 0;
					}
					else {
						_R(x, y)[0] = _I(x, y)[0];
						_R(x, y)[1] = _I(x, y)[1];
						_R(x, y)[2] = _I(x, y)[2];
					}
				}
			}
			res = _R;
			break;
		}
	}
	return res;

}







Segmentation::~Segmentation()
{
}
