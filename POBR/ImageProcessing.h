#pragma once
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <vector>

class ImageProcessing
{
public:
	ImageProcessing();
	~ImageProcessing();
	cv::Mat lowPassFilter(cv::Mat& I, int maskSize);
	cv::Mat medianFilter(cv::Mat& I, const int maskSize);
	cv::Mat meanFilter(cv::Mat& I, const int maskSize);
};

