#pragma once
#include "ImageProcessing.h"
#include "Segmentation.h"
#include "Word.h"
#include <list>

class ImageRecognition
{
public:
	ImageRecognition();
	~ImageRecognition();
	bool recognize(cv::Mat I);
	
private:
	long imageSize;
	int minSegmentSize;
	int maxSegmentSize;
	std::list<Segment> segments;
	std::list <Segment> Os, Rs, Ls, Es, Ns;

	void performFeatureExtraction();
	void performEarlyAnalysis();
	void filterSegmentList();
	void recognizeSegments();
	cv::Mat imageFromSegments(std::list<Segment> segments);
	void featureTesting();

	bool isO(Segment segment);
	bool isR(Segment segment);
	bool isL(Segment segment);
	bool isE(Segment segment);
	bool isN(Segment segment);

	bool isORLEN(cv::Mat I);
};

