#pragma once
#include "opencv2/imgproc/imgproc.hpp"
#include "ImageProcessing.h"
#include "Segment.h"
#include <iostream>
#include <list>
#include <time.h>


class Segmentation
{
public:
	Segmentation();
	~Segmentation();

	std::list<Segment> performSegmentation(cv::Mat& I);
	std::list<Segment> performSegmentationWithNoExtraction(cv::Mat& I);
	std::list<Segment> performSegmentationWithNoExtractionLessRestrictive(cv::Mat& I);

	std::list<Segment> getSegments(cv::Mat& I);
private:
	static const cv::Vec3b MIN_WHITE;
	static const cv::Vec3b MAX_WHITE;
	static const cv::Vec3b MIN_RED;
	static const cv::Vec3b MAX_RED;
	static const cv::Vec3b MIN_WHITE_LESS_RESTRICTIVE;

	cv::Mat extractFromBackground(cv::Mat& I, cv::Mat& flooded, uchar color);
	cv::Mat extractFromBackground(cv::Mat& I, cv::Mat& flooded, cv::Vec3b color);
	cv::Mat doFloodFill(cv::Mat& I, int x, int y, uchar oldColor, uchar newColor);
	cv::Mat doFloodFill(cv::Mat& I, int x, int y, cv::Vec3b oldColor, cv::Vec3b newColor);
	Segment doFloodFillSegmentation(cv::Mat& I, int x, int y, cv::Vec3b oldColor, cv::Vec3b newColor);
	cv::Mat extractRedAndConvertGray(const cv::Mat& I);
	cv::Mat extractPotentialText(const cv::Mat& I);
	cv::Mat extractPotentialTextLessRestrictive(const cv::Mat& I);
	bool isWhite(cv::Vec3b);
	bool isWhiteMoreless(cv::Vec3b);
	bool isRed(cv::Vec3b);
};

