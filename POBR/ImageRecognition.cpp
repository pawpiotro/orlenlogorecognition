#include "pch.h"
#include "ImageRecognition.h"



ImageRecognition::ImageRecognition(){

}

bool ImageRecognition::recognize(cv::Mat I) {
	Segmentation segmentation = Segmentation();
	segments = segmentation.performSegmentation(I);

	imageSize = I.cols * I.rows;
	minSegmentSize = 0.0001 * imageSize;
	maxSegmentSize = 0.07 * imageSize;
	std::cout << "Image size:" << imageSize << std::endl;
	std::cout << "Min segment size:" << minSegmentSize << std::endl;
	std::cout << "Max segment size:" << maxSegmentSize << std::endl;
	cv::Mat afterFiltration;

	filterSegmentList();
	performFeatureExtraction();
	afterFiltration = imageFromSegments(segments);
	cv::imshow("afterFiltration first attempt", afterFiltration);
	
	recognizeSegments();

	if (isORLEN(I)) {
		return true;
	}
	
	//if not found
	//try to omit extraction from background

	segments = segmentation.performSegmentationWithNoExtraction(I);
	filterSegmentList();
	performFeatureExtraction();
	afterFiltration = imageFromSegments(segments);
	cv::imshow("afterFiltration second attempt", afterFiltration);
	recognizeSegments();
	
	if (isORLEN(I)) {
		return true;
	}
	
	/*
	//if not found
	//try with less restrictive definition of white

	segments = segmentation.performSegmentationWithNoExtractionLessRestrictive(I);
	filterSegmentList();
	performFeatureExtraction();
	afterFiltration = imageFromSegments(segments);
	cv::imshow("afterFiltration third attempt", afterFiltration);
	recognizeSegments();

	if (isORLEN()) {
		return true;
	}

	*/

	//not found
	return false;
}

void ImageRecognition::recognizeSegments() {

	for (std::list<Segment>::iterator it = segments.begin(); it != segments.end(); ++it) {
		if (isO(*it))
			Os.push_back(*it);
		else if (isN(*it))
			Ns.push_back(*it);
		else if (isR(*it))
			Rs.push_back(*it);
		else if (isL(*it))
			Ls.push_back(*it);
		else if (isE(*it))
			Es.push_back(*it);
		
	}

	/*
	//Display classified objects
	cv::Mat IOs = imageFromSegments(Os);
	cv::Mat IRs = imageFromSegments(Rs);
	cv::Mat ILs = imageFromSegments(Ls);
	cv::Mat IEs = imageFromSegments(Es);
	cv::Mat INs = imageFromSegments(Ns);

	cv::imshow("Os", IOs);
	cv::imshow("Rs", IRs);
	cv::imshow("Ls", ILs);
	cv::imshow("Es", IEs);
	cv::imshow("Ns", INs);
	*/
}

void ImageRecognition::performEarlyAnalysis() {
	for (std::list<Segment>::iterator it = segments.begin(); it != segments.end(); ++it) {
		std::cout << it->getArea() << std::endl;
	}
}

void ImageRecognition::filterSegmentList() {
	std::list<Segment>::iterator it = segments.begin();
	double area;
	while(it != segments.end()){
		area = it->getArea();
		if(area < minSegmentSize || area > maxSegmentSize) {
			it = segments.erase(it);
		}
		else {
			it++;
		}
	}
}

void ImageRecognition::performFeatureExtraction() {
	for (std::list<Segment>::iterator it = segments.begin(); it != segments.end(); ++it) {
		it->extractFeatures();
		std::cout << "Color: " << it->color << " area:" << it->m00 << " height: " << it->height << " bounding box:" << it->minX << " " << it->minY << " " << it->maxX << " " << it->maxY <<
		std::endl << "			" << " MI1:" << it->MI1 << " MI2:" << it->MI2 << " MI3:" << it->MI3 << " MI4:" << it->MI4 << " MI7:" << it->MI7 << std::endl;
	}
}

cv::Mat ImageRecognition::imageFromSegments(std::list<Segment> segments) {
	cv::Mat I(1000, 2000, CV_8UC3);
	cv::Mat_<cv::Vec3b> _I = I;
	for (int i = 0; i < I.rows; ++i) {
		for (int j = 0; j < I.cols; ++j) {
			_I(i, j)[0] = 0;
			_I(i, j)[1] = 0;
			_I(i, j)[2] = 0;
		}
	}
	std::list<cv::Point2i> points;
	for (std::list<Segment>::iterator it = segments.begin(); it != segments.end(); ++it) {
		points = it->points;
		for (std::list<cv::Point2i>::iterator it2 = points.begin(); it2 != points.end(); ++it2) {
			_I(it2->y, it2->x) = it->color;
		}
	}
	I = _I;
	return I;
}

bool ImageRecognition::isO(Segment s) {
	if (s.MI1 > 0.2271 && s.MI1 < 0.2729 && s.MI2 > 0.000303 && s.MI2 < 0.023912 &&
		s.MI3 > 0.000000364 && s.MI3 < 0.00007913 && s.MI4 > 0.0000001656 && s.MI4 < 0.000005454
		&& s.MI7 > 0.01225 && s.MI7 < 0.15232) {
		return true;
	} else {
		return false;
	}
}

bool ImageRecognition::isR(Segment s) {
	if (s.MI1 > 0.20697 && s.MI1 < 0.26298 && s.MI2 > 0.001086 && s.MI2 < 0.027143 &&
		s.MI3 > 0.00000379 && s.MI3 < 0.00011207 && s.MI4 > 0.00000107 && s.MI4 < 0.0000378
		&& s.MI7 > 0.00933 && s.MI7 < 0.012954) {
		return true;
	}
	else {
		return false;
	}
}

bool ImageRecognition::isL(Segment s) {
	if (s.MI1 > 0.27009 && s.MI1 < 0.359085 && s.MI2 > 0.02647 && s.MI2 < 0.087506 &&
		s.MI3 > 0.005253 && s.MI3 < 0.009192 && s.MI4 > 0.000409 && s.MI4 < 0.0031337
		&& s.MI7 > 0.01035 && s.MI7 < 0.012104) {
		return true;
	}
	else {
		return false;
	}
}

bool ImageRecognition::isE(Segment s) {
	if (s.MI1 > 0.25282 && s.MI1 < 0.32878 && s.MI2 > 0.009 && s.MI2 < 0.056612 &&
		s.MI3 > 0.0005841 && s.MI3 < 0.001714 && s.MI4 > 0.0002467 && s.MI4 < 0.000887
		&& s.MI7 > 0.012263 && s.MI7 < 0.01441) {
		return true;
	}
	else {
		return false;
	}
}

bool ImageRecognition::isN(Segment s) {
	if (s.MI1 > 0.21622 && s.MI1 < 0.23207 && s.MI2 > 0.000382 && s.MI2 < 0.0095266 &&
		s.MI3 > 0.000002035 && s.MI3 < 0.00005833 && s.MI4 > 0.0000002426 && s.MI4 < 0.000008444
		&& s.MI7 > 0.01106 && s.MI7 < 0.013326) {
		return true;
	}
	else {
		return false;
	}
}

bool ImageRecognition::isORLEN(cv::Mat I) {
	std::list<Word> words;
	if (!Os.empty() && !Rs.empty() && !Ls.empty() && !Es.empty() && !Ns.empty()) {
		for (std::list<Segment>::iterator itO = Os.begin(); itO != Os.end(); ++itO) {
			Word word = Word();
			word.O = *itO;
			for (std::list<Segment>::iterator itR = Rs.begin(); itR != Rs.end(); ++itR) {
				if (itR->height > (word.O.height / 2) && itR->height < (word.O.height * 2) &&
					itR->minX > word.O.maxX && ((itR->maxX + itR->minX) / 2 - (word.O.maxX + word.O.minX) / 2) < (itR->height * 2)) {
					word.R = *itR;
					break;
				}
			}
			if (word.R.points.empty())
				continue;
			for (std::list<Segment>::iterator itL = Ls.begin(); itL != Ls.end(); ++itL) {
				if (itL->height > (word.R.height / 2) && itL->height < (word.R.height * 2) &&
					itL->minX > word.R.maxX && ((itL->maxX + itL->minX) / 2 - (word.R.maxX + word.R.minX) / 2) < (itL->height * 2)) {
					word.L = *itL;
					break;
				}
			}
			if (word.L.points.empty())
				continue;
			for (std::list<Segment>::iterator itE = Es.begin(); itE != Es.end(); ++itE) {
				if (itE->height > (word.L.height / 2) && itE->height < (word.L.height * 2) &&
					itE->minX > word.L.maxX && ((itE->maxX + itE->minX) / 2 - (word.L.maxX + word.L.minX) / 2) < (itE->height * 2)) {
					word.E = *itE;
					break;
				}
			}
			if (word.E.points.empty())
				continue;
			for (std::list<Segment>::iterator itN = Ns.begin(); itN != Ns.end(); ++itN) {
				if (itN->height > (word.E.height / 2) && itN->height < (word.E.height * 2) &&
					itN->minX > word.E.maxX && ((itN->maxX + itN->minX) / 2 - (word.E.maxX + word.E.minX) / 2) < (itN->height * 2)) {
					word.N = *itN;
					break;
				}
			}
			if (word.N.points.empty())
				continue;
			
			std::cout << "ORLEN logo found." << std::endl;
			words.push_back(word);
		}

		
		cv::Mat_<cv::Vec3b> _I = I;
		for (std::list<Word>::iterator it = words.begin(); it != words.end(); ++it) {
			it->boundingBox();
			int minX = it->minX;
			int maxX = it->maxX;
			int minY = it->minY;
			int maxY = it->maxY;
			for (int i = minX; i <= maxX; ++i) {
				_I(minY, i) = cv::Vec3b(0, 255, 255);
				_I(maxY, i) = cv::Vec3b(0, 255, 255);
			}
			for (int i = minY; i <= maxY; ++i) {
				_I(i, minX) = cv::Vec3b(0, 255, 255);
				_I(i, maxX) = cv::Vec3b(0, 255, 255);
			}
		}
		I = _I;
		cv::imshow("Image recognition result", I);
		return true;
	} 
	else {
		return false;
	}
}

void ImageRecognition::featureTesting() {

	double minM1 = 1.0f;
	double minM2 = 1.0f;
	double minM3 = 1.0f;
	double minM4 = 1.0f;
	double minM5 = 1.0f;
	double minM6 = 1.0f;
	double minM7 = 1.0f;
	double minM8 = 1.0f;
	double minM9 = 1.0f;
	double minM10 = 1.0f;

	double maxM1 = -1.0f;
	double maxM2 = -1.0f;
	double maxM3 = -1.0f;
	double maxM4 = -1.0f;
	double maxM5 = -1.0f;
	double maxM6 = -1.0f;
	double maxM7 = -1.0f;
	double maxM8 = -1.0f;
	double maxM9 = -1.0f;
	double maxM10 = -1.0f;

	for (std::list<Segment>::iterator it = segments.begin(); it != segments.end(); ++it) {
		if (it->MI1 < minM1)
			minM1 = it->MI1;
		if (it->MI2 < minM2)
			minM2 = it->MI2;
		if (it->MI3 < minM3)
			minM3 = it->MI3;
		if (it->MI4 < minM4)
			minM4 = it->MI4;
		if (it->MI5 < minM5)
			minM5 = it->MI5;
		if (it->MI6 < minM6)
			minM6 = it->MI6;
		if (it->MI7 < minM7)
			minM7 = it->MI7;
		if (it->MI8 < minM8)
			minM8 = it->MI8;
		if (it->MI9 < minM9)
			minM9 = it->MI9;
		if (it->MI10 < minM10)
			minM10 = it->MI10;

		if (it->MI1 > maxM1)
			maxM1 = it->MI1;
		if (it->MI2 > maxM2)
			maxM2 = it->MI2;
		if (it->MI3 > maxM3)
			maxM3 = it->MI3;
		if (it->MI4 > maxM4)
			maxM4 = it->MI4;
		if (it->MI5 > maxM5)
			maxM5 = it->MI5;
		if (it->MI6 > maxM6)
			maxM6 = it->MI6;
		if (it->MI7 > maxM7)
			maxM7 = it->MI7;
		if (it->MI8 > maxM8)
			maxM8 = it->MI8;
		if (it->MI9 > maxM9)
			maxM9 = it->MI9;
		if (it->MI10 > maxM10)
			maxM10 = it->MI10;
	}
	std::cout << std::endl
		<< "minM1: " << minM1 << " maxM1:" << maxM1 << std::endl
		<< "minM2: " << minM2 << " maxM2:" << maxM2 << std::endl
		<< "minM3: " << minM3 << " maxM3:" << maxM3 << std::endl
		<< "minM4: " << minM4 << " maxM4:" << maxM4 << std::endl
		<< "minM5: " << minM5 << " maxM5:" << maxM5 << std::endl
		<< "minM6: " << minM6 << " maxM6:" << maxM4 << std::endl
		<< "minM7: " << minM7 << " maxM7:" << maxM7 << std::endl
		<< "minM8: " << minM8 << " maxM8:" << maxM8 << std::endl
		<< "minM9: " << minM9 << " maxM9:" << maxM9 << std::endl
		<< "minM10: " << minM10 << " maxM10:" << maxM10 << std::endl;

}

ImageRecognition::~ImageRecognition()
{
}
