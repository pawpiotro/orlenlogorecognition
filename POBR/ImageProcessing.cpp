#include "pch.h"
#include "ImageProcessing.h"



ImageProcessing::ImageProcessing(){

}


ImageProcessing::~ImageProcessing()
{
}

cv::Mat ImageProcessing::lowPassFilter(cv::Mat& I, const int maskSize) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat res(I.rows, I.cols, CV_8UC3);
	int boundary = maskSize / 2;
	int maskSizeSquare = pow(maskSize, 2);
	float* mask = new float[maskSizeSquare];
	for (int i = 0; i < maskSizeSquare; i++) {
		mask[i] = 1.0; 
	}
	mask[maskSizeSquare / 2] = 4.0f;
	
	for (int i = 0; i < maskSize*maskSize; i++) {
		std::cout << mask[i] << " ";
	}
	std::cout << std::endl;
	

	switch (I.channels()) {
		case 3:
			cv::Mat_<cv::Vec3b> _I = I;
			cv::Mat_<cv::Vec3b> _R = res;
			for (int x = boundary; x < I.rows - boundary; ++x){
				for (int y = boundary; y < I.cols - boundary; ++y) {
					for (int k = 0; k < 3; k++) {
						int tmp = 0;
						int maskIdx = 0;
						for (int i = (-boundary); i < boundary; i++) {
							for (int j = (-boundary); j < boundary; j++) {
								tmp += _I(x + i, y + j)[k] * mask[maskIdx];
								maskIdx++;
							}
						}
						tmp = MIN(tmp, 255);
						tmp = MAX(tmp, 0);
						_R(x, y)[k] = tmp;
					}
				}
			}
			res = _R;
			break;
	}
	return res;
	
}

void insertSort(int arr[], int n){
	int a, key, b;
	for (a = 1; a < n; a++)
	{
		key = arr[a];
		b = a - 1;
		
		while (b >= 0 && arr[b] > key)
		{
			arr[b + 1] = arr[b];
			b = b - 1;
		}
		arr[b + 1] = key;
	}
}

cv::Mat ImageProcessing::medianFilter(cv::Mat& I, const int maskSize) {
	//input image in grayscale
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat res(I.rows, I.cols, CV_8UC1);
	int boundary = maskSize / 2;
	int maskSizeSquare = pow(maskSize, 2);
	switch (I.channels()) {
		case 1:
			cv::Mat_<uchar> _I = I;
			cv::Mat_<uchar> _R = res;
			for (int x = boundary; x < I.rows - boundary; ++x) {
				for (int y = boundary; y < I.cols - boundary; ++y) {
					int* tmparray = new int[maskSizeSquare];
					for (int i = 0; i < maskSize; i++) {
						for (int j = 0; j < maskSize; j++) {
							tmparray[i*j + j] = _I.at<uchar>(x + i - boundary, y + j - boundary);
						}
					}
					insertSort(tmparray, maskSizeSquare);
					_R.at<uchar>(x, y) = tmparray[maskSizeSquare/2];
				}
			}
			//edges
			for (int x = 0; x < boundary; ++x) {
				for (int y = 0; y < I.cols; ++y){
					_R.at<uchar>(x, y) = _I.at<uchar>(x, y);
					_R.at<uchar>(I.rows - x - 1, I.cols - y - 1) = _I.at<uchar>(I.rows - x -1, I.cols - y -1);
				}
			}
			for (int x = 0; x < I.rows; ++x) {
				for (int y = 0; y < boundary; ++y) {
					_R.at<uchar>(x, y) = _I.at<uchar>(x, y);
					_R.at<uchar>(I.rows - x - 1, I.cols - y - 1) = _I.at<uchar>(I.rows - x - 1, I.cols - y - 1);
				}
			}
			res = _R;
			break;
	}
	return res;

}


cv::Mat ImageProcessing::meanFilter(cv::Mat& I, const int maskSize) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat res(I.rows, I.cols, CV_8UC3);
	int boundary = maskSize / 2;
	int maskSizeSquare = pow(maskSize, 2);
	float* mask = new float[maskSizeSquare];
	for (int i = 0; i < maskSizeSquare; i++) {
		mask[i] = 1.0f / maskSizeSquare;
	}
	switch (I.channels()) {
		case 3:
			cv::Mat_<cv::Vec3b> _I = I;
			cv::Mat_<cv::Vec3b> _R = res;
			for (int x = boundary; x < I.rows - boundary; ++x) {
				for (int y = boundary; y < I.cols - boundary; ++y) {
					for (int k = 0; k < 3; k++) {
						int tmp = 0;
						int maskIdx = 0;
						for (int i = (-boundary); i < boundary; i++) {
							for (int j = (-boundary); j < boundary; j++) {
								tmp += _I(x + i, y + j)[k] * mask[maskIdx];
								maskIdx++;
							}
						}
						tmp = MIN(tmp, 255);
						tmp = MAX(tmp, 0);
						_R(x, y)[k] = tmp;
					}
				}
			}
			res = _R;
			break;
	}
	return res;

}